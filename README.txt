commerce_cybersource_hop
========================

Drupal Commerce Cybersource Hosted Order Page (HOP) module

You will need to download  the HOP.php from the cybersource business center.
Under "Tools & Settings" and "Hosted Order Page" click on "Security". Then under
the "Generate Security Script" heading sleect "php" then click "Submit". The
file that is downloaded will need to be placed inside one of the following locations
 - sites/*/libraries/commerce_cybersource_hop
 - the modules includes folder
Then you will need to open it up and add the following line
underneath "<?php": "namespace HOP;" then save the file.
